package com.test.weatherdata.repository;

import com.test.weatherdata.model.WeatherData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

//just a repository to store and perform crud operation on the data stored inside it
public interface WeatherRepository extends CrudRepository<WeatherData,Long> {
  /*  @Query(value = "Select * from WeatherData",nativeQuery = true)
    public List<WeatherData> findallweather();

    */

}
