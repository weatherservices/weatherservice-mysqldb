package com.test.weatherdata.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {



    @ExceptionHandler(EmptyInputException.class)
    public ResponseEntity<? > handleEmptyInput( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("601");
        er.setErrorMessage("Input Field Is Empty");
        er.setTimerStamp(new Date());
        return new ResponseEntity(er, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(WeatherDataException.class)
    public ResponseEntity<? > handleNoSuchElement( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("602");
        er.setErrorMessage("No such weather data found");
        er.setTimerStamp(new Date());
        return new ResponseEntity(er, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(ElementAreadyExistsException.class)
    public ResponseEntity<? > elementAlreadyExists( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("603");
        er.setErrorMessage("Element already Exists");
        er.setTimerStamp(new Date());
            return new ResponseEntity(er, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<? > handleNoSuchElementException( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("604");
        er.setErrorMessage("No such element is present in DB");
        er.setTimerStamp(new Date());
        return new ResponseEntity(er, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<? > handleHttpRequestMethodNotSupportedException( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("605");
        er.setErrorMessage("Wrong HTTP Method");
        er.setTimerStamp(new Date());
        return new ResponseEntity(er, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class) //TO HANDLE ANY GLOBAL EXCEPTION
    public ResponseEntity<? > handleException( WebRequest request){
        ErrorResponse er = new ErrorResponse();
        er.setErrorCode("611");
        er.setErrorMessage("Internal server error occured");
        er.setTimerStamp(new Date());
        return new ResponseEntity(er, HttpStatus.INTERNAL_SERVER_ERROR);
    }




}
