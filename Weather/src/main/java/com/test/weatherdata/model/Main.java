package com.test.weatherdata.model;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
//@Entity
@Embeddable
public class Main {

    public Main(Double temp, Double feels_like, Double temp_min, Double temp_max, Double pressure, Double humidity, Double sea_level, Double grnd_level) {
        this.temp = temp;
        this.feels_like = feels_like;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.humidity = humidity;
        this.sea_level = sea_level;
        this.grnd_level = grnd_level;
    }

//    @Id
    private Double temp;
    private Double feels_like;
    private Double temp_min;
    private Double temp_max;
    private Double pressure;
    private Double humidity;
    private Double sea_level;
    private Double grnd_level;
    public Main() {
    }
}