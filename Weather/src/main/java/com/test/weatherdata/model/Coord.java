package com.test.weatherdata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
@Embeddable
//@Entity
public class Coord {


    public Coord(Long lon, Long lat) {
        this.lon = lon;
        this.lat = lat;
    }

//    @Id
    private Long lon;
    private Long lat;
    public Coord() {
    }
}