package com.test.weatherdata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class WeatherData {

    private String cod;
    private Integer message;
    @Id
    private Long cnt;


    @OneToMany( cascade = CascadeType.ALL)
    private List<ForecastList> list;

    @OneToOne(cascade = CascadeType.ALL)
    private City city; //Embedding will also work- onetoone fails in terms of identical pkey containing records - obviously



    public WeatherData() {}
}



