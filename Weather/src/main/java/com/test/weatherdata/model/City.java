package com.test.weatherdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class City {

    @Id
    private Long id;

    private String name;

//    @OneToOne(cascade = CascadeType.ALL -> will also work
    @Embedded
    private Coord coord;

    private String country;

    private Integer population;

    private Long timezone;
    private Long sunrise;
    private Long sunset;


    public City() {
    }



    public City(Long id, String name, Coord coord, String country, Integer population, Long timezone, Long sunrise, Long sunset) {
        this.id = id;
        this.name = name;
        this.coord = coord;
        this.country = country;
        this.population = population;
        this.timezone = timezone;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

}