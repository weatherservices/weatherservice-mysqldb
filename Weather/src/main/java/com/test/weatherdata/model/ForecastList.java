package com.test.weatherdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "list")
public class ForecastList {
    @Id
    private Long dt;

//    @OneToOne(cascade = CascadeType.ALL)
    @Embedded
    private Main main; //same id fields so embeded

    @OneToMany(targetEntity = Weather.class, cascade = {CascadeType.MERGE,CascadeType.ALL})
    private List<Weather> weather; //has identical elements

//    @OneToOne(cascade = CascadeType.ALL)
    @Embedded
    private Clouds clouds; //has identical elements


//    @OneToOne(cascade = CascadeType.ALL)
    @Embedded
    private Wind wind; // identical IDS - SO DID EMBED

    private Long visibility;

    private Integer pop;

//    @OneToOne(cascade = {CascadeType.PERSIST})
    @Embedded
    private Sys sys; //has identical elements

    private String dt_txt;



}
