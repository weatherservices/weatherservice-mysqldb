package com.test.weatherdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
//@Entity
@Embeddable
public class Sys {

//  @Id
  private String pod;

    public Sys() {
    }

  public Sys(String pod) {
    this.pod = pod;
  }

}